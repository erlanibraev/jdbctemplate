package kz.ne.ktzh.utils.mapper;

import java.sql.ResultSet;

public abstract class RowMapper<T> {
	public abstract T mapRow(ResultSet rs, int rowNumber); 
}
