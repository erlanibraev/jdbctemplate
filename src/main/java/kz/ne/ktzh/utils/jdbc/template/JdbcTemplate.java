package kz.ne.ktzh.utils.jdbc.template;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import kz.ne.ktzh.utils.mapper.RowMapper;

public class JdbcTemplate<T> {
	
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public JdbcTemplate() {
		
	}
	
	public JdbcTemplate(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public List<T> query(String query, RowMapper<T> rowMapper) throws Exception {
		return  query(query, null, rowMapper);
	}
	
	public List<T> query(String query, Object[] parameters, RowMapper<T> rowMapper) throws Exception {
		List<T> result = new ArrayList<T>();
		Connection con=null;
		PreparedStatement ps = null;
		try {
			con = getConnection();
			ps = con.prepareStatement(query);
			if (parameters != null) {
				for(int i=0; i < parameters.length; i++) {
					ps.setObject(i, parameters[i]);
				}
			}
			ResultSet rs = ps.executeQuery();
			int i=0;
			while(rs.next()) {
				result.add(rowMapper.mapRow(rs, i));
			}
		} catch(Exception e) {
			throw new Exception(e);
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (con != null) {
				con.close();
			}
		}
		return result;
	}
	
	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
}
